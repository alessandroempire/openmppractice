#include <stdio.h>
#include <omp.h>

int main (int argc, char **argv){
 int tid, i, acum=0;
 #pragma omp parallel private(tid)
	 {
		 tid = omp_get_thread_num(); /* Get thread number */

		 #pragma omp for reduction(+:acum) /* split into threads */
		 for (i=0; i<10; i++) {
			 printf("Iter * thread (%d * %d) = %d\n", i, tid, i*tid);
			 acum = acum + i;
		 }
	 } 
	 /* All threads join master thread and terminate */
	 printf("Accumulated value = %d\n", acum);
}