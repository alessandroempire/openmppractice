/*
Written By Alessandro La Corte
*/


#include <stdio.h>
#include <stdlib.h>
#include <omp.h>

int main() {
	FILE * fp;
	int size;
	char buf[10000];
	
	/* read file “pru.txt” and store it in buf[] */
	/* NOTE: file must be smaller than 10000 characters */
	fp = fopen("pru.txt","r");
	fseek(fp, 0L, SEEK_END);
	size = ftell(fp);
	fseek(fp, 0L, SEEK_SET);
	fread (buf,1,size,fp);

	int acum=0; 
	int i=0; 

	#pragma omp parallel
	{

		#pragma omp for reduction(+:acum)
		for (i=0; i<size; i++){
		if (buf[i] == ' ')
			acum++;
		}
	}
	
	printf("Counter is %d \n", acum);
}


// Serial Code:

// int main() {
// 	FILE * fp;
// 	int size;
// 	char buf[10000];
	
// 	/* read file “pru.txt” and store it in buf[] */
// 	/* NOTE: file must be smaller than 10000 characters */
// 	fp = fopen("pru.txt","r");
// 	fseek(fp, 0L, SEEK_END);
// 	size = ftell(fp);
// 	fseek(fp, 0L, SEEK_SET);
// 	fread (buf,1,size,fp);
	
// 	/* add the code to count number of spaces in buf[] */
// 	int i =0; 
// 	int acum=0; 
// 	for (i=0; i<size; i++){
// 		if (buf[i] == ' ')
// 			acum++;
// 	}
// 	printf("Counter is %d \n", acum);
// }