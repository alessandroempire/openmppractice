#include <stdio.h>
#include <omp.h>

int main (int argc, char **argv){
	int tid;

	 /* Create several threads (OMP_NUM_THREADS) */
	 /* Each thread has a private variable (tid)*/
	 #pragma omp parallel private(tid)
	 {
		 tid = omp_get_thread_num(); /* Get thread number */
		 printf("Hello World from thread = %d\n", tid);
	 } 
	 /* All threads join master thread and terminate */
}

/**
gcc -fopenmp -o helloworld helloworld.c
*/