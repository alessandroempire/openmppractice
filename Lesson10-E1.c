/*
Written By Alessandro La Corte
*/

#include <stdio.h>
#include <time.h>
#include <mpi.h>

long num_steps = 100000;
double step = 1.0/100000.0;

int main() {

	clock_t begin = clock();

	int i, myid, size;
	double x, pi, local_sum = 0.0, sum=0.0;
	int send_vec[num_steps], recv_vect[num_steps]; 

	// Initialize the MPI environment 
	MPI_Init(NULL, NULL); 
	MPI_Comm_size(MPI_COMM_WORLD, &size); 
	MPI_Comm_rank(MPI_COMM_WORLD,&myid);

	if (myid ==0){
		int i=0; 
		for (i=0; i<num_steps;i++){
			send_vec[i]=i;
		}
	}

	MPI_Scatter(send_vec, num_steps/size, MPI_INT, recv_vect, 
		num_steps/size, MPI_INT, 0, MPI_COMM_WORLD);

	for(i = 0; i < num_steps/size; ++i) {
		x = (recv_vect[i]-0.5)*step;
	 	local_sum += 4.0/(1.0+x*x);
	}
	
	MPI_Reduce(&local_sum, &sum, 1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);

	if (myid == 0){

		pi = step*sum;
		printf("PI value = %f\n", pi);	
		
	}

	// Finalize the MPI environment. 
 	MPI_Finalize();

 	clock_t end = clock();

 	double run_time = (double)(end - begin) / CLOCKS_PER_SEC;

 	printf("Final time %f \n", run_time);
}




//Serial Code

// int main() {

// 	clock_t begin = clock();
// 	time_t start = time(NULL);

// 	int i;
// 	double x, pi, sum = 0.0;

// 	for(i = 0; i < num_steps; ++i) {
// 		x = (i-0.5)*step;
// 	 	sum += 4.0/(1.0+x*x);
// 	}

// 	pi = step*sum;
// 	printf("PI value = %f\n", pi);

// 	clock_t end = clock();
// 	time_t last = time(NULL);

// 	double run_time = (double)(end - begin) / CLOCKS_PER_SEC;

// 	printf("Final time %f \n", run_time);
// 	printf("Final time %f \n", (double) (last-start));

// }

