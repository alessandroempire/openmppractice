
#include <stdio.h>
#include <omp.h>

int main (int argc, char **argv){
 
 int tid;
 #pragma omp parallel private(tid)
 {
	 tid = omp_get_thread_num(); /* Get thread number */
	 #pragma omp sections
	 {
		 #pragma omp section
		 {printf("Section A on thread %d\n", tid);}
		 
		 #pragma omp section
		 {printf("Section B on thread %d\n", tid);}
	 }
 } 
 /* All threads join master thread and terminate */
}

/**
gcc -fopenmp -o helloworld_sec helloworld_sec.c
*/