#include <stdio.h>
#include <omp.h>
int main (int argc, char **argv){
 int tid,i;
 
 #pragma omp parallel private(tid)
 {
	 tid = omp_get_thread_num(); /* Get thread number */

	 #pragma omp for schedule (static) /* split into threads */
	 for (i=0; i<10; i++) {
	 	printf("Iteration %d on thread %d\n", i, tid);
	 }
 } 
 /* All threads join master thread and terminate */
}