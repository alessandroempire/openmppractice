#include <stdio.h>
#include <omp.h>

int main (int argc, char **argv){

 int tid, i=0, acum=0, global_acum=0;

 #pragma omp parallel private(tid,acum)
 {
	 tid = omp_get_thread_num(); /* Get thread number */
	 acum =0;
	 #pragma omp for /* split into threads */
	 for (i=0; i<10; i++) {
	 printf("Iter * thread (%d * %d) = %d\n", i, tid, i*tid);
		 acum = acum + (i*tid);
	 }
	 
	 #pragma omp critical 
	 global_acum = global_acum + acum;
 } 
 /* All threads join master thread and terminate */
 printf("Accumulated value = %d\n", acum);
 printf("Accumulated value1 = %d\n", global_acum);
}